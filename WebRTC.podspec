Pod::Spec.new do |s|
s.name              = 'WebRTC'
s.version           = '1.0'
s.summary           = 'Description of WebRTC Framework.'

s.description      = <<-DESC
A bigger description of WebRTC Framework.
DESC

s.homepage          = 'https://www.innoinstant.com'
s.license           = "MIT"
s.authors           = { 'Jagen K' => 'jagen@innocrux.com' }
s.source            = { :git => "https://bitbucket.org/Innocrux/innoinstant-ios-webrtc-sdk.git", :tag => s.version }
s.vendored_frameworks = 'WebRTC.xcframework'
s.ios.deployment_target = '11.0'


end